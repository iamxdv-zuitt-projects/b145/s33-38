const express = require('express');
const router = express.Router();
const courseController = require('../controllers/courseControllers');
const auth = require('../auth')


// Add a course

// router.post('/', (req, res) => {
// 	courseController.addCourse(req.body).then(resultFromController => res.send(resultFromController))
// });

router.post('/', auth.verify, (req, res) => {

	const data = {
		course : req.body,
		isAdmin : auth.decode(req.headers.authorization).isAdmin
	}
	courseController.addCourse(data).then(resultFromController => res.send(resultFromController))
});

// retrieve all courses
router.get('/all', auth.verify, (req, res) => {

	const data = auth.decode(req.headers.authorization)

	courseController.getAllCourses(data).then(resultFromController => res.send(resultFromController))
});

// retrieval of active courses
router.get('/', (req, res) => {
	courseController.getAllActive().then(resultFromController => res.send(resultFromController))
});

// retrieval of specific course 
router.get('/:courseId', (req, res) => {
	courseController.getCourse(req.params).then(resultFromController => res.send(resultFromController))
});

// update course
router.put('/:courseId', auth.verify, (req, res) => {

	const data = {
		courseId : req.params.courseId,
		payload : auth.decode(req.headers.authorization),
		updatedCourse : req.body
	}

	courseController.updateCourse(data).then(resultFromController => res.send(resultFromController))
});

/* ACTIVITY SOL'N to Archiving */

// Archiving Course

/*
//[SECTION] MY SOLUTION
	//Working-- Yes
router.put('/archive/:id', auth.verify, (req, res) => {

	const archive = {
		isActive : req.params.isActive
		
	}

	courseController.archiveCourse(archive).then(resultFromController => res.send(resultFromController))
});
*/

//[SECTION] Instructor's Solution
router.put('/:courseId/archive', auth.verify, (req, res) => {

	const data = {
		courseId : req.params.courseId,
		payload : auth.decode(req.headers.authorization)
	}

	courseController.archiveCourse(data).then(resultFromController => res.send(resultFromController))
});

module.exports = router;