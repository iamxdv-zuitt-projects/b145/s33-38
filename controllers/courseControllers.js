const Course = require('../models/Course');

//Add a course
/*
module.exports.addCourse = (reqBody) => {

	let newCourse = new Course({
		name : reqBody.name,
		description : reqBody.description,
		price : reqBody.price
	});

	return newCourse.save().then((course, err) => {

		if(err){
			return false;
		} else {
			return course;
		}
	})
}
*/


module.exports.addCourse = async (data) => {
	console.log(data)
		// User is an admin
		if (data.isAdmin) {
	
			// Creates a variable "newCourse" and instantiates a new "Course" object using the mongoose model
			// Uses the information from the request body to provide all the necessary information
			let newCourse = new Course({
				name : data.course.name,
				description : data.course.description,
				price : data.course.price
			});
	
			// Saves the created object to our database
			return newCourse.save().then((course, error) => {
	
				// Course creation successful
				if (error) {
	
					return false;
	
				// Course creation failed
				} else {
	
					return `Course added ${course}`;
	
				};
	
			});
	
		// User is not an admin
		} else {
			return false;
		};
		
	};
	
	// retrieving all courses
	
	module.exports.getAllCourses = async (user) => {
	
		if(user.isAdmin === true){
	
			return Course.find({}).then(result => {
	
				return result
			})
	
		} else {
	
			return `${user.email} is not authorized`
		}
	}
	
	// retrieval of active courses
	
	module.exports.getAllActive = () => {
		
		return Course.find({isActive : true}).then(result => {
	
			return result
		})
	}
	
	// retrieval of a specific course
	
	module.exports.getCourse = (reqParams) => {
	
		return Course.findById(reqParams.courseId).then(result => {
	
			return result
		})
	}
	
	// updating a course
	
	module.exports.updateCourse = (data) => {
		console.log(data);
		return Course.findById(data.courseId).then((result,err) => {
			console.log(result)
			if(data.payload.isAdmin === true){
			
					result.name = data.updatedCourse.name
					result.description = data.updatedCourse.description
					result.price = data.updatedCourse.price
		
				return result.save().then((updatedCourse, err) => {
					if(err){
						return false
					} else {
						return updatedCourse
					}
				})
			} else {
				return false
			}
		})
	}

/* ACTIVITY SOL'N to Archiving */
// Archive

/* 
//[SECTION] MY SOLUTION
	// Working Code-- Yes
	
module.exports.archiveCourse = (archive) => {
	console.log(archive);
	return Course.findById(archive.isActive).then((result,err) => {
		console.log(result)
		if(archive.isActive === false){
			
			result.isActive = archive.archiveCourse.isActive
			

		return result.delete().then((archiveCourse, err) => {
			if(err){
				return false
			} else {
				return archiveCourse
			}
		})
	} else {
		return true
	}
	})
}
*/

//[SECTION] Instructor's Solution

module.exports.archiveCourse = async (data) => {

	if(data.payload.isAdmin === true) {

		return Course.findById(data.courseId).then((result, err) => {

			result.isActive = false;

			return result.save().then((archivedCourse, err) => {

				if(err) {

					return false;

				} else {

					return result;
				}
			})
		})

	} else {

		return false;
	}
}
