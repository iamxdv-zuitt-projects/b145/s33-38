/*
========================A C T I V I T Y========================
1. Create a "USER" model with the following properties:
	a. firstName = String
	b. lastName = String
	c. email = String
	d. password = String
	e. isAdmin = Boolean (Default value - false)
	f. mobileNo- String
	g. enrollments- Array of objects
		i. courseId- String,
		ii. enrolledOn - Date (Default value - new Date object)
		iii. status - String (Default value - Enrolled)
2. Make sure that all the fields are required to ensure consistency in the user information being stored in our Database.
3. Create a git repository named s34-39
4. Initialize a local git repository, add the remote link and push to git with the commit message "Add activity code"
5. Add the link in the Boodle.
========================E N D========================
*/

//SOL'N:


const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({

	firstName : {
		type: String,
		required: [true, "First name is required."]
	},
	lastName : {
		type: String,
		required: [true, "Last Name is required."]
	},
	email : {
		type: String,
		required: [true, "email is required."]
	},
    password : {
		type: String,
		required: [true, "Password is required."]
    },
    isAdmin : {
		type: String,
		default: false
	},
	mobileNo : {
		type: String,
		required: [true, "Mobile Number is required."]
    },
    enrollments: [
        {
            courseId: {
                type: String,
                required: [true, "Course Id of enrollee is required"]
            },
            enrolledOn: {
                type: Date,
                default: new Date()
            },
			status : {
				type: String,
				default: "Enrolled"
			}
        }
    ]
});

module.exports = mongoose.model('User', userSchema);
